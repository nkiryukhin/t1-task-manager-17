package ru.t1.nkiryukhin.tm.component;

import ru.t1.nkiryukhin.tm.api.repository.ICommandRepository;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.command.project.*;
import ru.t1.nkiryukhin.tm.command.system.*;
import ru.t1.nkiryukhin.tm.command.task.*;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nkiryukhin.tm.exception.system.CommandNotSupportedException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.repository.CommandRepository;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;
import ru.t1.nkiryukhin.tm.service.*;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;


public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private void initDemoData() throws AbstractException {
        projectService.add(new Project("proj2", "project two", Status.IN_PROGRESS));
        projectService.add(new Project("proj1", "project one", Status.COMPLETED));
        projectService.add(new Project("proj4", "project four", Status.NOT_STARTED));
        projectService.add(new Project("proj3", "project three", Status.IN_PROGRESS));

        taskService.create("t2", "task two", Status.IN_PROGRESS);
        taskService.create("t1", "task one", Status.COMPLETED);
        taskService.create("t4", "task four", Status.NOT_STARTED);
        taskService.create("t3", "task three", Status.IN_PROGRESS);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void run(final String[] args) {
        try {
            initLogger();
            initDemoData();
            processArguments(args);
        } catch (final AbstractException e) {
            loggerService.error(e);
        }

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private boolean processArguments(final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(final String argument) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
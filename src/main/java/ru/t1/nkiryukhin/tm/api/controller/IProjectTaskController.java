package ru.t1.nkiryukhin.tm.api.controller;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;

}

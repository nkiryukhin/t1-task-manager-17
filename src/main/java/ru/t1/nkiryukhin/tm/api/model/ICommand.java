package ru.t1.nkiryukhin.tm.api.model;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException;

}

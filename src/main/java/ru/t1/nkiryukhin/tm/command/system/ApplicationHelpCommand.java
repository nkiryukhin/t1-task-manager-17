package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show command list";
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[HELP]");
        Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}

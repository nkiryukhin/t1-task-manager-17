package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show application version";
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }

}

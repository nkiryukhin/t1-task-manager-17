package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
